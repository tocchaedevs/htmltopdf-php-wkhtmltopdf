<?php

require_once("PrintRequest.php");
require_once("User.php");
require_once("PrintRequestProcessor.php");

require_once("Pdf.php");

/**
 * Print request processor for Business Cards
 */
class BCPrintRequestProcessor implements IPrintRequestProcessor {
	
	/**
	 * Empty constructor
	 */
	public function __construct() {	}
	
	public function processRequest(PrintRequest $printRequest): RequestProcessorResponse {
		
		// Load config file
		$config = parse_ini_file('./config.ini');
		
		// Create a new pdf converter
		$pdf = new Pdf(array(
				'binary' => $config['wkhtmltopdf_exe_path'],
				'ignoreWarnings' => true,
				'user-style-sheet' => $config['custom_stylesheet']
			)
		);
		
		// Load the html
		$pdf->addPage($printRequest -> getRequest());
		
		$printDate = date("YmdHms");
		
		// Construct final file name
		$filename = $config['print_file_path'] . $printRequest -> getRequestId() . "_" . $printDate . ".pdf";
		
		// If the pdf generation failed, return an error.
		if (!$pdf->saveAs($filename)) {
			return new RequestProcessorResponse(false, $pdf->getError());
			
			// Update the print request to failed
			$printRequest -> updateRequestStatus(PrintRequestStatus::FAILED);
		}
		
		// Update the print request to print successful
		$printRequest -> updateRequestStatusAndFile(PrintRequestStatus::PRINT_SUCCESSFUL, $filename);
		
		// if successful, then return a success flag, message and the filename of the generated pdf file
		return new RequestProcessorResponse(true, "Print Request Successfully Processed", $filename);
	}
}

?>