<?php

/*
Table DDL for print requests is:

CREATE TABLE `print_request` (
  `id` bigint AUTO_INCREMENT NOT NULL COMMENT 'Unique identifier of the table',
  `requester_id` bigint NOT NULL COMMENT 'ID of the user who requested the print.',
  `request_status` tinyint(4) NOT NULL COMMENT '0 = Failed to print, 1 = Successfully Printed, 2 = Pending, 3 = Processing, 4 = Successfully Sent to User',
  `request_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The date the print request was made',
  `request` longblob NOT NULL COMMENT 'HTML requested to be printed to pdf',
  `file_path` varchar(2000) COMMENT 'File path to the file that has been created',
  PRIMARY KEY (`id`),
  KEY `idx_request_status` (`request_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table stores related information for print requests';
*/

require_once("DB.php");

/**
 * RESTful websservice class for saving pdf print requests to the database
 */
class PrintRequest {
	
	private $requestId;
	private $requesterId;
	private $requestDateTime;
	private $request;
	
	/**
	 * Sets the objects attributes on construction of the object
	 */
	public function __construct($pRequestId, $pRequesterId, $pRequestDateTime, $pRequest) {
		$this->requestId = $pRequestId;
		$this->requesterId = $pRequesterId;
		$this->requestDateTime = $pRequestDateTime;
		$this->request = $pRequest;
	}
	
	/**
	 * Build all the necessary getter functions
	 */
	public function getRequestId() {
		return $this->requestId;
	}
	
	public function getRequesterId() {
		return $this->requesterId;
	}
	
	public function getRequest() {
		return $this->request;
	}
	
	/**
	 * Save request to the database and return true if successful or false if not
	 */
	public function savePrintRequest(){
		$db = new DB();
		
		$vRequestDateTime = $db -> quote($this->requestDateTime);
		$vRequest = $db -> quote($this->request);
		
		$sql = "INSERT INTO `print_request` (`requester_id`, `request_datetime`, `request_status`, `request`) ".
									"VALUES (" . $this->requesterId . ", " . $vRequestDateTime . ", " . PrintRequestStatus::PENDING . ", " . $vRequest . ")";
									
		$result = $db -> query($sql);
		
		return $result;
	}
	
	/**
	 * Update the request to processing
	 */
	public function updateRequestStatus($requestStatus){
		$db = new DB();
		
		$sql = "UPDATE `print_request` SET `request_status` = " . $requestStatus . " WHERE `id` = " . $this->requestId;
									
		$result = $db -> query($sql);
		
		return $result;
	}
	
	/**
	 * Update the request to processing
	 */
	public function updateRequestStatusAndFile($requestStatus, $fullFilePath){
		$db = new DB();
		
		$vFullFilePath = $db -> quote($fullFilePath);
		
		$sql = "UPDATE `print_request` SET `request_status` = " . $requestStatus . ", `file_path` = " . $vFullFilePath . " WHERE `id` = " . $this->requestId;
									
		$result = $db -> query($sql);
		
		return $result;
	}
}

abstract class PrintRequestStatus {
    const FAILED = 0;
    const PRINT_SUCCESSFUL = 1;
	const PENDING = 2;
	const PROCESSING = 3;
	const SENT_TO_USER = 4;
}
?>