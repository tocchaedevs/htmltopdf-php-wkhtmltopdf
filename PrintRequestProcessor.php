<?php

require_once("PrintRequest.php");
require_once("User.php");

/**
 * Interface for class print type processors
 */
interface IPrintRequestProcessor {
    public function processRequest(PrintRequest $printRequest): RequestProcessorResponse;
}

/**
 * Class to define the request processor's response
 */
class RequestProcessorResponse {
	
	private $success;
	private $message;
	private $filename;
	
	public function __construct($pSuccess, $pMessage, $pFilename = NULL) {
        $this->success = $pSuccess;
		$this->message = $pMessage;
		$this->filename = $pFilename;
    }
	
	/**
	 * Build all the necessary getter functions
	 */
	public function isSuccessful() {
		return $this->success;
	}
	
	public function getMessage() {
		return $this->message;
	}
	
	public function getFilename() {
		return $this->filename;
	}
}

?>