<?php

error_reporting(E_ALL);

require_once("DB.php");
require_once("PrintRequest.php");
require_once("User.php");
require_once("BCPrintRequestProcessor.php");

class PrintRequestProcessorThread extends Thread {
    public $printRequest;

    public function __construct($pPrintRequest) {
        $this->printRequest = $pPrintRequest;
    }

    public function run() {
		// Call the BUSINESS_CARD processing
		$processor = new BCPrintRequestProcessor();
		$processorResponse = $processor -> processRequest($this->printRequest);
		
		echo $processorResponse -> getMessage() . "<br>";
    }
}

?>