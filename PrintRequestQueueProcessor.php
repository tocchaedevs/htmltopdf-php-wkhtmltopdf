<?php

require_once("DB.php");
require_once("PrintRequest.php");
require_once("PrintRequestProcessorThread.php");

$db = new DB();

// Load config file
$config = parse_ini_file('./config.ini');

// Get the number of requests to be processed per batch 
$scheduleProcessBatch = $config['schedule_process_batch'];

// Select query to retrieve all pending requests from the database
$query = "SELECT * FROM `print_request` where `request_status` = 2  order by `id` ASC, `request_datetime` ASC LIMIT " . $scheduleProcessBatch;

// Retrieve the records from the database
$results = $db -> select($query);

// Check if the result was unsuccessful
if ($results === false) {
	echo "The select query could not be run." . "<br>";
} else {
	
	echo "Started at: " . date("Y-m-d H:m:s") . "<br>";
	
	// Loop through the rows returned and start a thread for each row to process the request.
	foreach ($results as &$row) {
		// Create the request object
		$printRequest = new PrintRequest($row['id'], $row['requester_id'], $row['request_datetime'], $row['request']);
		
		// Update the row and set the status to processing
		$printRequest -> updateRequestStatus(PrintRequestStatus::PROCESSING);
		
		// Start a separate thread to process this print request
		$requestThread = new PrintRequestProcessorThread($printRequest);
		$requestThread -> start();
		
		echo "The the row is: " . $row['id'] . ", " . $row['requester_id'] . ". Thread started." . "<br>";
	}
	
	echo "Ended at: " . date("Y-m-d H:m:s") . "<br>";
}

?>