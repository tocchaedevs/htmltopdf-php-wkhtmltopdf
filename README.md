# HTML to PDF Converter

This project is an HTML to pdf converter.

You can simply clone the project on your local machine.

To test it you need to make sure that you have the print_request table created in your mysql database. You will find
the create DDL statement in the "PrintRequest.php" file of the project.

There is a config.ini file. Make sure that the configurations in this file are set according to your environment.

There is also a sample HTML card template provided in this project and a css file as well. You will find these in the
templates and css directories of this project respectively.

To test sample print requests, run the "testPrintRequests.php" file in your browser or on command line.

Then to test the processing, run the "PrintRequestQueueProcessor.php" on the command line.

ENJOY!!!

## To Use This Product:

1. Make sure you have threading installed for your php server
2. Make sure you wkhtmltopdf installed on your server as well
3. Make sure you create the table in your database as specified above
4. Make sure that your server can run cron jobs

## How it works?

This product works with what we call Print Requests. These are requests that are saved in the database that will be picked up by a cron job and generate a pdf file based on the html that the request was saved with. The cron job will then pick up the request, and change the status of this request in the database to **"processing"** right away. The records retrieved are then passed on to the pdf processor which will then covert each html into pdf, and place this pdf file inside a specified directory. The processor will update the request in the database with the status **"successfully printed"** or **"failed"** accordingly. If it is successful, the full path of the generated file will be saved to the database on the print request's record, and you may then use this path to create a docwnload link if you wish or attach it to an email.

## How to integrate into your website:

1. Copy all the .php files and place them in your php website directory, preferably in a folder called htmltopdf
2. To save a PrintRequest to the database, this is how you do it:
    	
		....
		
		/* Import the print requests class */
		require_once("PrintRequest.php");
		
		....
		
		/**
		 * Create the request object here.
		 * The requester ID here would be the ID of the user logged in making the request.
		 * The htmlParsed would be the full HTML generated that you want to be converted to pdf
		 */
		$printRequest = new PrintRequest(NULL, $requesterId, date("Y-m-d H:m:s"), $htmlParsed);
		
		/* Save the request to the DB */
		$result = $printRequest -> savePrintRequest();
	
3. Then create a cron job that will run the the PrintRequestQueueProcessor.php in the background on the server.
4. Make sure to setup the config.ini file with the correct configurations according to your server setup and installations.