<?php

/**
 * User Object class
 */
class User {
	
	private $userId;
	private $firstName;
	private $lastName;
	private $email;
	private $phoneNumber;
	private $industry;
	private $skillProfession;
	private $country;
	private $stateProvince;
	
	/**
	 * Sets the objects attributes on construction of the object
	 */
	public function __construct($pUserId, $pFirstName, $pLastName, $pEmail, $pPhoneNumber, 
								$pIndustry, $pSkillProfession, $pCountry, $pStateProvince) {
		$this->userId = $pUserId;
		$this->firstName = $pFirstName;
		$this->lastName = $pLastName;
		$this->email = $pEmail;
		$this->phoneNumber = $pPhoneNumber;
		$this->industry = $pIndustry;
		$this->skillProfession = $pSkillProfession;
		$this->country = $pCountry;
		$this->stateProvince = $pStateProvince;
	}
	
	/**
	 * Build all the necessary getter functions
	 */
	public function getUserId() {
		return $this->userId;
	}
	
	public function getFirstName() {
		return $this->firstName;
	}
	
	public function getLastName() {
		return $this->lastName;
	}
	
	public function getEmail() {
		return $this->email;
	}
	
	public function getPhoneNumber() {
		return $this->phoneNumber;
	}
	
	public function getIndustry() {
		return $this->industry;
	}
	
	public function getSkillProfession() {
		return $this->skillProfession;
	}
	
	public function getCountry() {
		return $this->country;
	}
	
	public function getStateProvince() {
		return $this->stateProvince;
	}
}
?>