-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2017 at 07:22 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biznesxpo`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `animated__videos`
--

CREATE TABLE `animated__videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `banner__themes`
--

CREATE TABLE `banner__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bizcard__themes`
--

CREATE TABLE `bizcard__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brochure__themes`
--

CREATE TABLE `brochure__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `business__profiles`
--

CREATE TABLE `business__profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `continuation_sheet__themes`
--

CREATE TABLE `continuation_sheet__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cover_letter__themes`
--

CREATE TABLE `cover_letter__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cv__themes`
--

CREATE TABLE `cv__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `c_d__themes`
--

CREATE TABLE `c_d__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `c_i__manuals`
--

CREATE TABLE `c_i__manuals` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `design__orders`
--

CREATE TABLE `design__orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download_p_d_f__orders`
--

CREATE TABLE `download_p_d_f__orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `download__printable__designs`
--

CREATE TABLE `download__printable__designs` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `envelope__themes`
--

CREATE TABLE `envelope__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fb_datas`
--

CREATE TABLE `fb_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `flyer__themes`
--

CREATE TABLE `flyer__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `google_datas`
--

CREATE TABLE `google_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `greeting_card__themes`
--

CREATE TABLE `greeting_card__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `interests`
--

CREATE TABLE `interests` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice__themes`
--

CREATE TABLE `invoice__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `letter_head__themes`
--

CREATE TABLE `letter_head__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `linked_in_datas`
--

CREATE TABLE `linked_in_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logo__themes`
--

CREATE TABLE `logo__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(259, '2017_10_07_202510_create_users_table', 1),
(260, '2017_10_15_181839_create_visitors_table', 1),
(261, '2017_10_15_185748_create_fb_datas_table', 1),
(262, '2017_10_15_185813_create_tw_datas_table', 1),
(263, '2017_10_15_185852_create_google_datas_table', 1),
(264, '2017_10_15_185932_create_linked_in_datas_table', 1),
(265, '2017_10_15_190018_create_user_datas_table', 1),
(266, '2017_10_15_190459_create_interests_table', 1),
(267, '2017_10_15_190651_create_histories_table', 1),
(268, '2017_10_15_191335_create_activities_table', 1),
(269, '2017_10_15_191643_create_theme__customizations_table', 1),
(270, '2017_10_15_191727_create_bizcard__themes_table', 1),
(271, '2017_10_15_192258_create_flyer__themes_table', 1),
(272, '2017_10_15_192321_create_letter_head__themes_table', 1),
(273, '2017_10_15_192355_create_invoice__themes_table', 1),
(274, '2017_10_15_192523_create_quotation__themes_table', 1),
(275, '2017_10_15_192549_create_cv__themes_table', 1),
(276, '2017_10_15_192656_create_portfolio__themes_table', 1),
(277, '2017_10_15_192727_create_receipt__themes_table', 1),
(278, '2017_10_15_193058_create_profile_card__themes_table', 1),
(279, '2017_10_15_193507_create_logo__themes_table', 1),
(280, '2017_10_15_193610_create_banner__themes_table', 1),
(281, '2017_10_15_194309_create_envelope__themes_table', 1),
(282, '2017_10_15_194330_create_continuation_sheet__themes_table', 1),
(283, '2017_10_15_194433_create_brochure__themes_table', 1),
(284, '2017_10_15_194522_create_c_d__themes_table', 1),
(285, '2017_10_15_195132_create_c_i__manuals_table', 1),
(286, '2017_10_15_195540_create_animated__videos_table', 1),
(287, '2017_10_15_195807_create_greeting_card__themes_table', 1),
(288, '2017_10_15_200602_create_cover_letter__themes_table', 1),
(289, '2017_10_15_200716_create_sample_docs_table', 1),
(290, '2017_10_15_200943_create_professional__profiles_table', 1),
(291, '2017_10_15_201000_create_business__profiles_table', 1),
(292, '2017_10_15_201232_create_design__orders_table', 1),
(293, '2017_10_15_201351_create_download_p_d_f__orders_table', 1),
(294, '2017_10_15_201709_create_download__printable__designs_table', 1),
(295, '2017_10_15_202537_create_user_profiles_table', 1),
(296, '2017_10_15_202656_create_user_transactions_table', 1),
(297, '2017_10_15_203612_create_public__blog_posts_table', 1),
(298, '2017_10_15_203708_create_public__careers_table', 1),
(299, '2017_10_15_203727_create_public__helps_table', 1),
(300, '2017_10_15_203813_create_public__pages_table', 1),
(301, '2017_10_15_203858_create_tags_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio__themes`
--

CREATE TABLE `portfolio__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `professional__profiles`
--

CREATE TABLE `professional__profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_card__themes`
--

CREATE TABLE `profile_card__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `public__blog_posts`
--

CREATE TABLE `public__blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `public__careers`
--

CREATE TABLE `public__careers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `public__helps`
--

CREATE TABLE `public__helps` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `public__pages`
--

CREATE TABLE `public__pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation__themes`
--

CREATE TABLE `quotation__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `receipt__themes`
--

CREATE TABLE `receipt__themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sample_docs`
--

CREATE TABLE `sample_docs` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `theme__customizations`
--

CREATE TABLE `theme__customizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tw_datas`
--

CREATE TABLE `tw_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `status` enum('n','y','s','b') NOT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `skill_profession` varchar(255) DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  `state_province` varchar(255) DEFAULT NULL,
  `city_town` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `onboarding` enum('0','1','2','3','4') NOT NULL,
  `last_login` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verifyToken` varchar(255) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `remember_me` enum('false','true') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `password`, `email`, `phone_no`, `username`, `gender`, `birthday`, `status`, `industry`, `skill_profession`, `country`, `state_province`, `city_town`, `street`, `postal_code`, `onboarding`, `last_login`, `created_at`, `updated_at`, `verifyToken`, `remember_token`, `remember_me`) VALUES
(1, 'Chris Bode', 'Adewole', '$2y$10$aZTFkF0H6XnotjOhRpQ7Se2vr8yrTb65SGrdPsenlbAu9/y6tpvP.', 'bodilum@gmail.com', '+27733110149', 'none', 'male', '1983-05-15 00:00:00', 'y', 'Information Technology', 'Mobile | Web | UIUX', 'South Africa', 'Free State', 'Randburg', '316 Surrey Avenue, Ferndale', 'none', '4', 1509198822, '2017-10-26 19:58:50', '2017-10-28 11:53:42', NULL, 'ZUCMY0JyrpAhqtrkcjAMNqhbkN7CBlLDvOHrKnismfEDPIBcMEAHXPxzUacA', 'true'),
(2, 'Adeoluwa Chris', 'Adewole', '$2y$10$zBVEziMqPJIz53tET3DB2.QkgAwT314DzL1ifCunUGY.BC1WyHqbW', 'bodilum@bodilum.com', 'none', NULL, 'male', '1990-05-15 00:00:00', 'n', 'Information Technology', 'Graphics Designer', 'South Africa', 'Gauteng', 'Randburg', '316 Surrey Avenue, Ferndale', 'none', '4', 1509205676, '2017-10-28 11:54:13', '2017-10-28 13:47:56', 'SmfFHwK4FVJXqL3G02UjfOmAKRgV3n2DmcDP8zes', '2X7Gjo27SeABktHIkckh57XEeiAjnV4p7rrX5NIr', 'false');

-- --------------------------------------------------------

--
-- Table structure for table `user_datas`
--

CREATE TABLE `user_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_transactions`
--

CREATE TABLE `user_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animated__videos`
--
ALTER TABLE `animated__videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner__themes`
--
ALTER TABLE `banner__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bizcard__themes`
--
ALTER TABLE `bizcard__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brochure__themes`
--
ALTER TABLE `brochure__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business__profiles`
--
ALTER TABLE `business__profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `continuation_sheet__themes`
--
ALTER TABLE `continuation_sheet__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cover_letter__themes`
--
ALTER TABLE `cover_letter__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv__themes`
--
ALTER TABLE `cv__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_d__themes`
--
ALTER TABLE `c_d__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `c_i__manuals`
--
ALTER TABLE `c_i__manuals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `design__orders`
--
ALTER TABLE `design__orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_p_d_f__orders`
--
ALTER TABLE `download_p_d_f__orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download__printable__designs`
--
ALTER TABLE `download__printable__designs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `envelope__themes`
--
ALTER TABLE `envelope__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fb_datas`
--
ALTER TABLE `fb_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flyer__themes`
--
ALTER TABLE `flyer__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `google_datas`
--
ALTER TABLE `google_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `greeting_card__themes`
--
ALTER TABLE `greeting_card__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interests`
--
ALTER TABLE `interests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice__themes`
--
ALTER TABLE `invoice__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `letter_head__themes`
--
ALTER TABLE `letter_head__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linked_in_datas`
--
ALTER TABLE `linked_in_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo__themes`
--
ALTER TABLE `logo__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio__themes`
--
ALTER TABLE `portfolio__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `professional__profiles`
--
ALTER TABLE `professional__profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_card__themes`
--
ALTER TABLE `profile_card__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `public__blog_posts`
--
ALTER TABLE `public__blog_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `public__careers`
--
ALTER TABLE `public__careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `public__helps`
--
ALTER TABLE `public__helps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `public__pages`
--
ALTER TABLE `public__pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation__themes`
--
ALTER TABLE `quotation__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt__themes`
--
ALTER TABLE `receipt__themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sample_docs`
--
ALTER TABLE `sample_docs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme__customizations`
--
ALTER TABLE `theme__customizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tw_datas`
--
ALTER TABLE `tw_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `user_datas`
--
ALTER TABLE `user_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_transactions`
--
ALTER TABLE `user_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `animated__videos`
--
ALTER TABLE `animated__videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banner__themes`
--
ALTER TABLE `banner__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bizcard__themes`
--
ALTER TABLE `bizcard__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brochure__themes`
--
ALTER TABLE `brochure__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `business__profiles`
--
ALTER TABLE `business__profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `continuation_sheet__themes`
--
ALTER TABLE `continuation_sheet__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cover_letter__themes`
--
ALTER TABLE `cover_letter__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cv__themes`
--
ALTER TABLE `cv__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `c_d__themes`
--
ALTER TABLE `c_d__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `c_i__manuals`
--
ALTER TABLE `c_i__manuals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `design__orders`
--
ALTER TABLE `design__orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `download_p_d_f__orders`
--
ALTER TABLE `download_p_d_f__orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `download__printable__designs`
--
ALTER TABLE `download__printable__designs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `envelope__themes`
--
ALTER TABLE `envelope__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fb_datas`
--
ALTER TABLE `fb_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flyer__themes`
--
ALTER TABLE `flyer__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `google_datas`
--
ALTER TABLE `google_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `greeting_card__themes`
--
ALTER TABLE `greeting_card__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interests`
--
ALTER TABLE `interests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice__themes`
--
ALTER TABLE `invoice__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `letter_head__themes`
--
ALTER TABLE `letter_head__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `linked_in_datas`
--
ALTER TABLE `linked_in_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logo__themes`
--
ALTER TABLE `logo__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT for table `portfolio__themes`
--
ALTER TABLE `portfolio__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `professional__profiles`
--
ALTER TABLE `professional__profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profile_card__themes`
--
ALTER TABLE `profile_card__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `public__blog_posts`
--
ALTER TABLE `public__blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `public__careers`
--
ALTER TABLE `public__careers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `public__helps`
--
ALTER TABLE `public__helps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `public__pages`
--
ALTER TABLE `public__pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation__themes`
--
ALTER TABLE `quotation__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt__themes`
--
ALTER TABLE `receipt__themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sample_docs`
--
ALTER TABLE `sample_docs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `theme__customizations`
--
ALTER TABLE `theme__customizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tw_datas`
--
ALTER TABLE `tw_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_datas`
--
ALTER TABLE `user_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_transactions`
--
ALTER TABLE `user_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
