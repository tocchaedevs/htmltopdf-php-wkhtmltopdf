<?php

require_once("PrintRequest.php");
require_once("User.php");

/**
 * Get the user details from the database
 */
function getUser($userId) {
	
	$db = new DB();
	
	$user = NULL;
	
	// Select query to get the user for a given id
	$query = "SELECT * FROM `users` where `id` = " . $userId;
	
	// Retrieve the user's record from the database
	$result = $db -> select($query);
	
	// Make sure the results has something in it.
	if ($result) {
		$row = $result[0];
		
		$user = new User($row['id'], $row['first_name'], $row['last_name'], 
						 $row['email'], $row['phone_no'], $row['industry'], 
						 $row['skill_profession'], $row['country'], 
						 $row['state_province']);
		
	}
	
	return $user;
}

echo "Started at: " . date("Y-m-d H:m:s") . "<br>";

// Load config file
$config = parse_ini_file('./config.ini');

// Load the html file template
$html = file_get_contents($config['card_template_file']);

// For Loop to save print requests to the database for testing
for ($i = 1; $i <= 100; $i++) {
	// Randomly pick who to print the details for
	$requesterId = rand(1, 2);
	
	// Get the user from the DB
	$user = getUser($requesterId);
	
	// setup the html to be parsed
	$htmlParsed = $html;
	
	// Setup the parameters
	$params = ["first_name" => $user -> getFirstName(), 
			   "last_name" => $user -> getLastName(), 
			   "skill_profession" => $user -> getSkillProfession(),
			   "industry" => $user -> getIndustry(),
			   "phone_number" => $user -> getPhoneNumber(),
			   "email" => $user -> getEmail(),
			   "province_state" => $user -> getStateProvince(),
			   "country" => $user -> getCountry()
			  ];
	
	// Replace the parameters in the html
	foreach($params as $key => $value) {
		$htmlParsed = str_replace("{".$key."}", $value, $htmlParsed);
	};
	
	// Create the request object
	$printRequest = new PrintRequest(NULL, $requesterId, date("Y-m-d H:m:s"), $htmlParsed);
	
	// Save the request to the DB
	$result = $printRequest -> savePrintRequest();
	
	echo $i . ': ' . $requesterId . ' - ' . $result . "<br>";
}

echo "Ended at: " . date("Y-m-d H:m:s") . "<br>";

?>